//
//  ControllerViewController.h
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 14/3/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+VGParallaxHeader.h"
#import "AppDelegate.h"

@interface ControllerViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, BLEDelegate>
@property (strong, nonatomic ) AppDelegate *app;
@property (strong , nonatomic) IBOutlet UITableView *table;

@end
