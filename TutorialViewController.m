//
//  TutorialViewController.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "TutorialViewController.h"
#import "AppDelegate.h"

#define RGBCOLOR(r,g,b)     [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

@interface TutorialViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgView3;
@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    // Title Setting
    self.view.backgroundColor  = [UIColor clearColor];
    
    // Content Setting
    self.txtContent.editable = NO;

//    NSLog(@"%f", self.view.frame.size.width);
//    UIImage *img3 = [UIImage imageNamed:@"desktop_mock.png"];
//    UIImageView* imgView3 = [[UIImageView alloc] initWithFrame:_imagepost.bounds];
//    imgView3.center = CGPointMake(_imagepost.bounds.size.width/2, _imagepost.bounds.size.height/2);
//    imgView3.backgroundColor    = UIColor.redColor;
//    imgView3.image = img3;
//    imgView3.contentMode = UIViewContentModeScaleAspectFill;
    
    _lblTitle.text = _setTitle;
    _txtContent.text = _setContent;
    [_imgView2 setImage:[UIImage imageNamed:_setImage2]];

}

-(void)openURL:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://nature-internet.ivehost.net/NGC/index.html"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
