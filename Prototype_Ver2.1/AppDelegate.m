//
//  AppDelegate.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "AppDelegate.h"
#import "HelpViewController.h"
#import "ConnectViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate (){
    CLLocation* location;
    NSString *number;
}
@end

@implementation AppDelegate
CLLocationManager* locationManager =nil;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // 啟動時間
    sleep(2.5);
    
    //檢查是否第一次開啟
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
            NSLog(@"第一次開啓");
        HelpViewController  *helpVC = [[HelpViewController alloc] init];
        self.window.rootViewController = helpVC;
    } else {
        NSLog(@"不是第一次開啓");
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    
//    BLE Setting
    _bleShield = [[BLE alloc] init];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    

    
    [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(locationSetUp) userInfo:nil repeats:YES];
    
   
    
    
    [NSTimer scheduledTimerWithTimeInterval:120.f target:self selector:@selector(disconnectBLE) userInfo:nil repeats:NO];
    

    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)disconnectBLE{
    if (_bleShield.activePeripheral) {
        if (_bleShield.activePeripheral.state == CBPeripheralStateConnected) {
        [[_bleShield CM] cancelPeripheralConnection:[_bleShield activePeripheral]];
        }
    }
}

-(void)notifyAlerm{
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    //      Notification
       if (notifyAlarm) {
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 1;
        notifyAlarm.alertBody = [NSString stringWithFormat: @"你附近有棵樹喎，過去玩下啦 \n %@", number];
        [locationManager stopUpdatingLocation];
        [app scheduleLocalNotification:notifyAlarm];
    }
}

-(void)locationSetUp{
    NSLog(@"開始定位");
    //      Location
    if (nil == locationManager){
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.pausesLocationUpdatesAutomatically = NO;
    }
    [locationManager startMonitoringSignificantLocationChanges];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager stopMonitoringSignificantLocationChanges];

}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"定位錯誤： \n%@", error);
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location = [locations lastObject];
    NSString* latloc =[NSString stringWithFormat:@"\n 緯度= %.8f \n 經度= %.8f", location.coordinate.latitude, location.coordinate.longitude];
    NSLog(@"經緯度:%@", latloc);
    number = [NSString stringWithFormat:@"%@", latloc];
    
    if (location.coordinate.latitude >= 22.387000 && location.coordinate.latitude <= 22.395000){
        [self notifyAlerm];
    }
//    [locationManager stopUpdatingLocation];

}

@end
