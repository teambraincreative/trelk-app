//
//  ViewController.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.blurStyle = UIBlurEffectStyleDark;

}

- (IBAction)dismissClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
