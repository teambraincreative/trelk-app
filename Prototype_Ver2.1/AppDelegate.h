//
//  AppDelegate.h
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLE.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, BLEDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property BLE *bleShield;
@end

