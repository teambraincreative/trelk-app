//
//  ConnectViewController.h
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 27/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeRippleButton.h"
#import "BLE.h"

@interface ConnectViewController : UIViewController <BLEDelegate, UIAlertViewDelegate>
@end
