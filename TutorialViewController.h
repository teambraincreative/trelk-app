//
//  TutorialViewController.h
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <NSURLConnectionDataDelegate>

@property (assign, nonatomic) NSInteger index;

@property NSString *setTitle;
@property NSString *setContent;
@property NSString *setImage;
@property NSString *setImage2;


@property (assign, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgView2;

@end
