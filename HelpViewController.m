//
//  HelpViewController.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "HelpViewController.h"
#import "TutorialViewController.h"
#import "ConnectViewController.h"
#import "UIView+MaterialDesign.h"

#define RGBCOLOR(r,g,b)     [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
@interface HelpViewController ()

@property (strong, nonatomic) TutorialViewController *tutPage;
@property (strong, nonatomic) ConnectViewController  *conPage;

// Data for tutorial page
@property (strong, nonatomic) NSArray *arrayTitle;
@property (strong, nonatomic) NSArray *arrayImage2;
@property (strong, nonatomic) NSArray *arrayImage;
@property (strong, nonatomic) NSArray *arrayContent;

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _conPage = [ConnectViewController new];
    
    // Set tutorial page content
    _arrayTitle = @[@"WELCOME", @"FIRST", @"THEN", @"ENJOY"];
    _arrayContent = @[@"Thank you for download our app. We hope you will enjoy it.",@"Go to our tree and you can find the location on our website. ", @"Please turn on your bluetooth and start to scan the tree.", @"If you want to know more about us. Please visit our website."];
    _arrayImage2 = @[@"icon.png", @"icon.png", @"icon.png", @"icon.png"];
    
    // Page Controller Setting
    _pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    TutorialViewController *initialViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    // Skip Button Setting
    _btnSkip = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnSkip setTitle:@"SKIP" forState:UIControlStateNormal];
    self.btnSkip.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    self.btnSkip.backgroundColor = [UIColor clearColor];
    [self.btnSkip setTintColor: RGBCOLOR(222, 222, 222)];
    self.btnSkip.frame = CGRectMake(20, self.view.bounds.size.height-36.5, 50, 36.5);
    [self.btnSkip addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnSkip];
    
    // Done Button Setting
    _btnDone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    self.btnDone.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    self.btnDone.backgroundColor = [UIColor clearColor];
    [self.btnDone setTintColor: RGBCOLOR(222, 222, 222)];
    self.btnDone.frame = CGRectMake(self.view.bounds.size.width-70, self.view.bounds.size.height-36.5, 50, 36.5);
    [self.btnDone addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnDone];
    [self.btnDone setHidden:YES];
    
    
}

#pragma mark Back Action
- (void)backAction:(UIButton *)sender event:(UIEvent *)event {
    CGPoint exactTouchPosition = [[[event allTouches] anyObject] locationInView:self.view];
    [UIView mdInflateTransitionFromView:self.view toView:_conPage.view originalPoint:exactTouchPosition duration:0.7 completion:^{
        //[self.view removeFromSuperview];
        [[UIApplication sharedApplication]delegate].window.rootViewController = _conPage;
    }];
}

-(void)goBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (TutorialViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    TutorialViewController *childViewController = [[TutorialViewController alloc] initWithNibName:@"TutorialViewController" bundle:nil];
    
    if (([self.arrayTitle count] == 0) || (index >= [self.arrayTitle count])) {
        return nil;
    }
    
    childViewController.setTitle = _arrayTitle[index];
    childViewController.setContent = _arrayContent[index];
    childViewController.setImage = _arrayImage [index];
    childViewController.setImage2 = _arrayImage2[index];
    childViewController.index = index;

    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialViewController *)viewController index];

    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialViewController *)viewController index];

    index++;

    if (index == self.arrayTitle.count) {
        [self.btnSkip setHidden:YES];
        [self.btnDone setHidden:NO];
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

//頁數
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return self.arrayTitle.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

@end
