//
//  ControllerViewController.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 14/3/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "ControllerViewController.h"
#import "ConnectViewController.h"
#import "HeaderView.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "UIScrollView+VGParallaxHeader.h"
#import "BLE.h"

#define RGBCOLOR(r,g,b)     [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

@interface ControllerViewController (){
    NSString *command;
    NSString *actionCommand;
    NSString *sayGoodbye;
    NSData *sendData;
}
@property (strong, nonatomic)  AVAudioPlayer *sound;
@property (strong, nonatomic) HeaderView *headerView;
@property (strong, nonatomic) NSArray *questionList0;
@property (strong, nonatomic) NSArray *questionList1;
@property (strong, nonatomic) NSArray *questionList2;
@property (strong, nonatomic) NSArray *questionList3;
@property (strong, nonatomic) NSArray *questionList4;
@property (strong, nonatomic) NSMutableArray *questionTable;
@end

@implementation ControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorColor = UIColor.clearColor;
    
    _app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _app.bleShield.delegate = self;
    
    // Button sound effect
//    NSString *pathsoundFile = [[NSBundle mainBundle] pathForResource:@"button-09" ofType:@"mp3"];
//    _sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:pathsoundFile] error:NULL];
    
    _questionList0 = [[NSMutableArray alloc] initWithObjects:@"WHY", @"Why are you here?", nil];
    _questionList1 = [[NSMutableArray alloc] initWithObjects:@"WHAT", @"What are your function?", @"What is the humidity?", @"What is the temperature?", nil];
    _questionList2 = [[NSMutableArray alloc] initWithObjects:@"WHO", @"Who are you?", nil];
    _questionList3 = [[NSMutableArray alloc] initWithObjects:@"WHERE", @"Where is here?", nil];
    _questionList4 = [[NSMutableArray alloc] initWithObjects:@"HOW", @"How are you?", @"How about the air?", nil];
    
    _questionTable = [[NSMutableArray alloc] initWithObjects:_questionList0, _questionList1, _questionList2, _questionList3, _questionList4, nil];
    NSLog(@"%@", _questionTable);
    
    self.headerView = [HeaderView instantiateFromNib];
    self.headerView.backgroundColor = RGBCOLOR(255, 255, 255);
    UIImageView *bgPic = [[UIImageView alloc] initWithFrame:CGRectMake(30, 32, 210, 172)];
    NSString *imagePath = [[NSBundle mainBundle]pathForResource:@"tree_v2" ofType:@"png"];
    bgPic.image =[UIImage imageWithContentsOfFile:imagePath];
    bgPic.layer.zPosition =12;
    [self.table addSubview:bgPic];
    
    UILabel *nameBLE = [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2+50, 100, 100, 100)];
    NSString *bleName = _app.bleShield.activePeripheral.name;
    nameBLE.text = [NSString stringWithFormat:@"%@", bleName];
    [nameBLE setFont:[UIFont fontWithName:@"Roboto-Light" size:36]];
    nameBLE.backgroundColor = [UIColor clearColor];
    nameBLE.textColor = [UIColor whiteColor];
    nameBLE.layer.zPosition =12;
    [self.table addSubview:nameBLE];

    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    btnClose.frame = CGRectMake(self.view.bounds.size.width-70, 14, 56, 56);
    UIImage *image = [UIImage imageNamed:@"cross.png"];
    CGSize sacleSize = CGSizeMake(24, 24);
    UIGraphicsBeginImageContextWithOptions(sacleSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, sacleSize.width, sacleSize.height)];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    resizedImage = [resizedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    btnClose.adjustsImageWhenHighlighted = NO;
    [btnClose setImage:resizedImage forState:UIControlStateNormal & UIControlStateHighlighted];
    [btnClose setTintColor:[UIColor whiteColor]];
    btnClose.layer.cornerRadius = btnClose.layer.bounds.size.width/2;
    [self.headerView addSubview:btnClose];
    btnClose.layer.zPosition =12;
    [btnClose addTarget:self action:@selector(alertLeave) forControlEvents:UIControlEventTouchUpInside];
    
    [self.table setParallaxHeaderView:self.headerView
                                 mode:VGParallaxHeaderModeTopFill
                               height:200];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.table shouldPositionParallaxHeader];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)alertLeave{
    UIAlertView *alertdisconnect = [[UIAlertView alloc] initWithTitle:@"Disconnect" message:@"Are you sure leave now?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes", @"No", nil];
    [alertdisconnect show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0: // Say YES
                    // 斷線前講聲ByeBye
            if (_app.bleShield.activePeripheral) {
                if (_app.bleShield.activePeripheral.state == CBPeripheralStateConnected) {
                    sayGoodbye = [NSString stringWithFormat:@"9"];
                    sendData = [sayGoodbye dataUsingEncoding:NSUTF8StringEncoding];
                    [_app.bleShield write:sendData];
                    [[_app.bleShield CM] cancelPeripheralConnection:[_app.bleShield activePeripheral]];
            }
                }
            
                break;
            
        default:
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_questionTable count] ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_questionTable objectAtIndex:section] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSLog(@"\n Section: %ld \n Row: %ld \n Text: %@", (long)indexPath.section, (long)indexPath.row, [[_questionTable objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]);
    
    //編寫執行碼
   actionCommand = [NSString stringWithFormat:@"%ld%ld", (long)indexPath.section, (long)indexPath.row];
    if ([actionCommand isEqualToString:@"12"]){
        command = [NSString stringWithFormat:@"2"];
    } else if ([actionCommand isEqualToString:@"13"]) {
        command = [NSString stringWithFormat:@"1"];
    } else if  ([actionCommand isEqualToString:@"42"]){
        command= [NSString stringWithFormat:@"5"];
    }
    
    //傳送執行碼
    sendData = [command dataUsingEncoding:NSUTF8StringEncoding];
    [_app.bleShield write:sendData];
    
    NSLog(@"傳送指令: %@", actionCommand);
    if  (indexPath.row == 0) {
        
    } else {
    [self performSegueWithIdentifier:@"show" sender:self];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.textLabel.text = [[_questionTable objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    switch (indexPath.section){
        default:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.font = [UIFont fontWithName:@"Roboto-Light" size:36];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.textLabel.textColor = UIColor.darkGrayColor;
                    break;
                default:
                    cell.textLabel.font = [UIFont fontWithName:@"Roboto-Light" size:24];
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;
                    cell.textLabel.textColor = UIColor.grayColor;
                    break;
            }
            
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}

-(void)bleDidDisconnect{
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"停止連線，返回連接頁。");
    }];
//    ConnectViewController *firstView = [self.storyboard instantiateViewControllerWithIdentifier:@"ConnectPage"];
//    [self presentViewController:firstView animated:YES completion:nil];
    

    
}

@end
