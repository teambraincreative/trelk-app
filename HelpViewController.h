//
//  HelpViewController.h
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 24/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController <UIPageViewControllerDataSource, NSURLConnectionDataDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (retain, nonatomic) UIColor *pageIndicatorTintColor;
@property (retain, nonatomic) UIColor *currentPageIndicatorTintColor;
@property (nonatomic, readonly) UIButton	*btnSkip;
@property (nonatomic, readonly) UIButton	*btnDone;
@end
