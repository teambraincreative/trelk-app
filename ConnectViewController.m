//
//  ConnectViewController.m
//  Prototype_Ver2.1
//
//  Created by Man Wing Fai on 27/2/15.
//  Copyright (c) 2015 Man Wing Fai. All rights reserved.
//

#import "ConnectViewController.h"
#import "ControllerViewController.h"
#import "HelpViewController.h"
#import "AppDelegate.h"

#import "PulsingHaloLayer.h"
#import "UIView+MaterialDesign.h"
#import "MMMaterialDesignSpinner.h"
// Framework
#import <CoreBluetooth/CoreBluetooth.h>
#import <AVFoundation/AVFoundation.h>

#define RGBCOLOR(r,g,b)     [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

@interface ConnectViewController ()
@property (retain, nonatomic) UILabel *lblTitle;
@property (retain, nonatomic) UIButton *btnHelp;
@property (retain, nonatomic) UIButton *btnScan;
@property (strong, nonatomic)  AVAudioPlayer *sound;
@property AppDelegate *app;
@property (strong, nonatomic) PulsingHaloLayer    *halo;
@property (readonly, nonatomic) HelpViewController *helpPage;
@property (strong, nonatomic) ControllerViewController *controlPage;
@property (strong, nonatomic)     MMMaterialDesignSpinner *spinnerView;

@end

@implementation ConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _helpPage = [HelpViewController new];
    
//    UIImageView *bgPic = [[UIImageView alloc] initWithFrame:self.view.frame];
//    NSString *imagePath = [[NSBundle mainBundle]pathForResource:@"bg" ofType:@"png"];
//    bgPic.image =[UIImage imageWithContentsOfFile:imagePath];
//    [self.view addSubview:bgPic];
    
    UIImageView *mount = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-self.view.bounds.size.width/1.8, self.view.bounds.size.width, self.view.bounds.size.width/1.7)];
    NSString *imagePath2 = [[NSBundle mainBundle]pathForResource:@"mountain" ofType:@"png"];
    mount.image =[UIImage imageWithContentsOfFile:imagePath2];
    [self.view addSubview:mount];
    
//    UIImageView *treePic = [[UIImageView alloc] initWithFrame:CGRectMake(43,  self.view.bounds.size.height-178, 160, 160/0.8376)];
//    treePic.image=[UIImage imageNamed:@"tree.png"];
//    [self.view addSubview:treePic];

    
    // Initialize the progress view
    _spinnerView = [[MMMaterialDesignSpinner alloc] initWithFrame:CGRectMake(self.view.center.x-20, self.view.center.y-40 , 40, 40)];
    _spinnerView.lineWidth = 5.f;
    //[_spinnerView setCenter:CGPointMake(self.view.center.x, self.view.center.y)];
    _spinnerView.backgroundColor = [UIColor clearColor];
    _spinnerView.tintColor = [UIColor whiteColor];
    [self.view addSubview:_spinnerView];
    
    // Create the button;
    [self titleLable];
    [self helpButton];
    [self scanButton];
}

-(void)viewDidAppear:(BOOL)animated{
     // Call BLE function
    _app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_app.bleShield controlSetup];
    _app.bleShield.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)titleLable{
    //Title Label Setting
    _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x, self.view.center.y-200, 250, 170)];
    [_lblTitle setCenter: CGPointMake(self.view.center.x, self.lblTitle.center.y)];
    _lblTitle.textAlignment = NSTextAlignmentCenter;
    _lblTitle.font = [UIFont fontWithName:@"Roboto-Light" size:98.0];
    _lblTitle.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _lblTitle.numberOfLines = 0;
    _lblTitle.text = @"Trelk";
    _lblTitle.textColor = [UIColor whiteColor];
    _lblTitle.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_lblTitle];
}

-(void)helpButton{
    // Help Button Setting
    _btnHelp =[UIButton buttonWithType:UIButtonTypeCustom];
    _btnHelp.frame = CGRectMake(self.view.bounds.size.width-66.5, 26.5, 50, 36.5);
    [self.btnHelp setTitle:@"HELP" forState:UIControlStateNormal];
    [self.btnHelp.titleLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:18.0]];
    [self.btnHelp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnHelp.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnHelp setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_btnHelp];
    [self.btnHelp addTarget:self action:@selector(goTotut) forControlEvents:UIControlEventTouchUpInside];
}

-(void)scanButton{
    // Scan Button Setting
    _btnScan = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnScan.frame = CGRectMake(self.view.center.x, self.view.center.y, 100, 100);
    [_btnScan  setCenter: CGPointMake(self.view.center.x, _btnScan.center.y)];
    [self.btnScan setTitle:@"SCAN" forState:UIControlStateNormal];
    [self.btnScan.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:35.0]];
    [self.btnScan setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    _btnScan.layer.cornerRadius = self.btnScan.layer.bounds.size.width/2;
//    [self.btnScan setBackgroundColor:RGBCOLOR(33,150,243)];
//    [self.btnScan.layer setShadowColor:[UIColor colorWithWhite:0 alpha:0.68].CGColor];
//    [self.btnScan.layer setShadowOffset:CGSizeMake(0, 2)];
//    [self.btnScan.layer setShadowOpacity:0.7];
//    [self.btnScan.layer setShadowRadius:3];
    //[self.btnScan.layer setZPosition:16];
    [self.view addSubview:self.btnScan];
    [self.btnScan addTarget:self action:@selector(beginToScanPeripheral:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Help Page Action
- (void)goToTutorialPage:(UIButton *)sender event:(UIEvent *)event {
    CGPoint exactTouchPosition = [[[event allTouches] anyObject] locationInView:self.view];
    [UIView mdInflateTransitionFromView:self.view toView:self.helpPage.view originalPoint:exactTouchPosition duration:0.7 completion:^(void) {
        [[UIApplication sharedApplication]delegate].window.rootViewController = self.helpPage;
    }];
}

-(void)goTotut{
    HelpViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutPage"];
    [self presentViewController:mainVC animated:YES completion:nil];
}

#pragma mark - Scan the BLE
- (void)beginToScanPeripheral:(id)sender
{
    // Button sound effect
//    NSString *pathsoundFile = [[NSBundle mainBundle] pathForResource:@"button-09" ofType:@"mp3"];
//    _sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:pathsoundFile] error:NULL];
//    [_sound play];
    [_spinnerView startAnimating];

//    if (self.halo) {
//        [self.halo removeFromSuperlayer];
//    }
//    self.halo = [PulsingHaloLayer layer];
//    self.halo.position = self.btnScan.center;
//    self.halo.backgroundColor = UIColor.grayColor.CGColor;
//    [self.view.layer insertSublayer:self.halo below:self.btnScan.layer];
    [self.btnHelp setEnabled:NO];    //lock the button
    [self.btnScan setEnabled:NO];    //lock the button
    [_app.bleShield findBLEPeripherals:5];    // Timeout: 5 seconds
    [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(connectToBLETimer:) userInfo:nil repeats:NO];
    NSLog(@"開始搜尋可連接的BLE。");
}

#pragma mark - Connect the BLE
- (void)connectToBLETimer:(NSTimer *)timer
{
    //[self.halo removeFromSuperlayer];
    [_spinnerView stopAnimating];
    // If find it, try to connect
    if (_app.bleShield.peripherals.count > 0) {
        [_app.bleShield connectPeripheral:[_app.bleShield.peripherals objectAtIndex:0]];
        NSLog(@"找到可用BLE，並且開給連接。。。");
    }
    else {
    UIAlertView *connectError = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Device Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [connectError show];
    NSLog(@"附近沒有可用BLE！");
    }
    [self.btnScan setEnabled:YES];
    [self.btnHelp setEnabled:YES];
    
    ConnectViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainPage"];
    [self presentViewController:mainVC animated:YES completion:nil];

}

#pragma mark - If connect Successfully
- (void)bleDidConnect
{
    NSLog(@"連接成功，進入主控制頁。");
    // Go to next page
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    ControllerViewController  *mainVC = [storyboard instantiateViewControllerWithIdentifier:@"MainPage"];
//    mainVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    mainVC.modalPresentationStyle = UIModalPresentationFullScreen;
    ConnectViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainPage"];
    [self presentViewController:mainVC animated:YES completion:^{
    
    // connected - say hello to user
    NSString *sayHello= [NSString stringWithFormat:@"0"];
    NSData *sendData = [sayHello dataUsingEncoding:NSUTF8StringEncoding];
        [_app.bleShield write:sendData];
        
    }];
    
}

@end
